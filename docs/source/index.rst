.. Structures de données en Python documentation master file, created by
   sphinx-quickstart on Thu Nov  8 10:58:33 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

M1103 - Structures de données (en python)
=========================================


.. toctree::
   :caption: les sujets de TD et TP
   :name: semaines
   :maxdepth: 2
   :glob:
   
   tp1
   feuilles_2018/*



.. toctree::
   :caption: tous les exercices
   :name: synthese
   :maxdepth: 2
   :glob:

   exercices/*/*

   


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
