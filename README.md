# SDD

Créer un répertoire :

    $ mkdir sdd
    $ cd sdd

Cloner le dépot : 

    $ git clone --recurse https://gitlab.com/cprevost/sdd.git .

Créer un virtualenv

    $ virtualenv -p python3 venv

Activer le vistualenv

    $ source venv/bin/activate
   
Installer les dépendances :

    (venv)$ pip install -r easyShinx/requirements.txt

Puis dans le dossier ``docs`` : 

    $ make html

ou bien

    $ make latexpdf



Mettre à jour le dépot puis aller sur ```http://info.iut45.univ-orleans.fr/dashboard/``` 





# Aide pour la création d'un nouveau projet 
 
## Initier le projet

Initier un dépot sur gitLab (Attention : le projet doit être **public**)

Dans ce dépot, créer un repertoire docs dans lequel on lance la procédure d'installation rapide :

    $ mkdir docs

    $ cd docs

    $ sphinx-quickstart

Générez les pages html
   
    $ make html


## Publier le projet

Aller sur ``http://info.iut45.univ-orleans.fr/`` (la première fois, il faut se créer un compte)

Il faut ensuite **Import a Project** soit en choisissant un projet dans le menu, soit en cliquant sur **Import Manually**

Attention !! Votre projet doit être public !

Au bout de quelques minutes, le projet est prêt

Pour visualiser le html > **View your documentation** ou bien aller sur ``http://info.iut45.univ-orleans.fr/docs/monProjet/en/latest/``








